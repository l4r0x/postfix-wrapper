const {
    app
} = require('electron');

const fs = require('fs')
const path = require('path')

function configPath () {
    return path.join(app.getPath('appData'), 'config.json')
}

exports.load = function () {
    let config
    try {
        config = JSON.parse(fs.readFileSync(configPath(), 'utf8'))
    } catch (e) {
        console.log("Loading data: " + e)
        config = {}
    }
    return config
}

exports.save = function (config) {
    try {
        fs.writeFileSync(configPath(), JSON.stringify(config))
    } catch (e) {
        console.log("Saving data: " + e)
    }
}

